function notice
{
    message=$1
    echo "[Notice]: ${message}"
}

function error
{
    message=$1
    echo "[Error]: ${message}"
    exit
}

function get_answer
{
    default=$1
    if [[ ${default} == "" ]]; then
        read -p "> " ans
        echo ${ans}
    else
        read -p "[default ${default}]> " ans
        if [[ ${ans} == "" ]]; then
            ans=${default}
        fi
        echo ${ans}
    fi
}
