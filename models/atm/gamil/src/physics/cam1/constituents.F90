#include <misc.h>
#include <params.h>

module constituents

    use shr_kind_mod, only: r8 => shr_kind_r8
    use physconst,    only: r_universal
    use string
    use console

    implicit none

    private

    save

    public cnst_init
    public cnst_add      ! add a constituent to the list of advected (or nonadvected) constituents
    public cnst_get_ind  ! get the index of a constituent
    public cnst_chk_dim  ! check that number of constituents added equals dimensions (pcnst, pnats)

    integer, public :: pcnst  ! number of advected constituents (including water vapor)
    integer, public :: pnats  ! number of non-advected constituents
    integer, public :: ppcnst ! total number of constituents

    integer, parameter, public :: advected = 0         ! type value for constituents which are advected
    integer, parameter, public :: nonadvec = 1         ! type value for constituents which are not advected

    character(8), public, allocatable :: cnst_name(:)          ! constituent names (including any non-advected)
    character(128), public, allocatable :: cnst_longname(:)    ! long name of constituents

    ! Constants for each tracer
    real(r8), public, allocatable :: cnst_cp  (:)             ! specific heat at constant pressure (J/kg/K)
    real(r8), public, allocatable :: cnst_cv  (:)             ! specific heat at constant volume (J/kg/K)
    real(r8), public, allocatable :: cnst_mw  (:)             ! molecular weight (kg/kmole)
    real(r8), public, allocatable :: cnst_rgas(:)             ! gas constant ()
    real(r8), public, allocatable :: qmin     (:)             ! minimum permitted constituent concentration (kg/kg)
    real(r8), public, allocatable :: qmincg   (:)             ! for backward compatibility only

    ! Volume mixing ratios for trace gases at the Earth's surface
    real(r8), public :: co2vmr                        ! co2   volume mixing ratio 
    real(r8), public :: n2ovmr                        ! n2o   volume mixing ratio 
    real(r8), public :: ch4vmr                        ! ch4   volume mixing ratio 
    real(r8), public :: f11vmr                        ! cfc11 volume mixing ratio 
    real(r8), public :: f12vmr                        ! cfc12 volume mixing ratio 

    integer, private :: padv = 0 ! index pointer to last advected tracer
    integer, private :: pnad     ! index pointer to last non-advected tracer

contains

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   Initialize constituents module (allocate memories).
    !
    ! Authors:
    !
    !   Li Dong <dongli@lasg.iap.ac.cn> - 2013-02-13
    ! --------------------------------------------------------------------------
    
    subroutine cnst_init()

        character(50), parameter :: sub_name = "cnst_init"

        ppcnst = pcnst+pnats
        pnad = pcnst

        allocate(cnst_name(ppcnst))
        allocate(cnst_longname(ppcnst))
        allocate(cnst_cp(ppcnst))
        allocate(cnst_cv(ppcnst))
        allocate(cnst_mw(ppcnst))
        allocate(cnst_rgas(ppcnst))
        allocate(qmin(ppcnst))
        allocate(qmincg(ppcnst))

        call notice("Initialize constituents module.", sub_name)

    end subroutine cnst_init

    subroutine cnst_add(name, type, mwc, cpc, qminc, ind, longname)

        character(*), intent(in) :: name
        character(*), intent(in), optional :: longname

        integer,  intent(in) :: type   ! flag indicating advected or nonadvected
        real(r8), intent(in) :: mwc    ! constituent molecular weight (kg/kmol)
        real(r8), intent(in) :: cpc    ! constituent specific heat at constant pressure (J/kg/K)
        real(r8), intent(in) :: qminc  ! minimum value of mass mixing ratio (kg/kg)
                                       ! normally 0., except water 1.E-12, for radiation.

        integer, intent(out) :: ind    ! global constituent index (in q array)

        character(50), parameter :: sub_name = "cnst_add"

        if (type == advected) then
            ! set tracer index and check validity, advected tracer
            padv = padv+1
            ind  = padv
            if (padv > pcnst) then
                call error("Advected tracer index exceeds pcnst "//trim(tr(pcnst))//"!", sub_name, __LINE__)
            end if
            call notice("Add advected tracer "//trim(name)//" at index "//trim(tr(ind))//".", sub_name)
        else if (type == nonadvec) then
            ! set tracer index and check validity, non-advected tracer
            pnad = pnad+1
            ind  = pnad
            if (pnad > ppcnst) then
                call error("Non-advected tracer index exceeds pcnst+pnats "//trim(tr(ppcnst))//"!", sub_name, __LINE__)
            end if
            call notice("Add non-advected tracer "//trim(name)//" at index "//trim(tr(ind))//".", sub_name)
        else
            ! unrecognized type value
            call error("Input type flag """//trim(tr(type))//""" invalid!", sub_name, __LINE__)
        end if

        ! set tracer name and constants
        cnst_name(ind) = name
        if (present(longname)) then
            cnst_longname(ind) = longname
        else
            cnst_longname(ind) = name
        end if

        cnst_cp(ind) = cpc
        cnst_mw(ind) = mwc
        qmin   (ind) = qminc
        qmincg (ind) = qminc
        if (ind == 1) qmincg = 0.  ! This crap is replicate what was there before ****

        cnst_rgas(ind) = r_universal*mwc
        cnst_cv  (ind) = cpc-cnst_rgas(ind)

    end subroutine cnst_add

    subroutine cnst_get_ind (name, ind)

        character(len=*), intent(in) :: name ! constituent name
        integer, intent(out) :: ind ! global constituent index (in q array)

        integer :: m

        character(50), parameter :: sub_name = "cnst_get_ind"

        ! Find tracer name in list
        do m = 1, ppcnst
            if (name == cnst_name(m)) then
                ind  = m
                return
            end if
        end do

        ! Unrecognized name
        call error("Name """//trim(name)//""" is not found in list!", sub_name, __LINE__)

    end subroutine cnst_get_ind

    subroutine cnst_chk_dim

        if (padv /= pcnst) then
            call error("Advected tracer number "//trim(tr(padv))//" is not equal to pcnst "//trim(tr(pcnst)))
        end if
        if (pnad /= ppcnst) then
            call error("Non-advected tracer number "//trim(tr(pnad))//" is not equal to pcnst+pnats "//trim(tr(ppcnst)))
        end if

    end subroutine cnst_chk_dim

end module constituents
