module vertical_coordinate

    ! ##############
    ! OLD GAMIL PART
    use shr_kind_mod, only: r8 => shr_kind_r8
    ! ##############

    implicit none

    real(r8), allocatable :: hyai(:)
    real(r8), allocatable :: hybi(:)
    real(r8), allocatable :: hyam(:)
    real(r8), allocatable :: hybm(:)

    real(r8), allocatable :: hypi(:)       ! reference pressures at interfaces
    real(r8), allocatable :: hypm(:)        ! reference pressures at midpoints

    real(r8) ps0         ! base state sfc pressure for level definitions
    real(r8) pmtop

    real(r8), allocatable :: sig (:)
    real(r8), allocatable :: sigl(:)
    real(r8), allocatable :: dsig(:)

contains

    ! --------------------------------------------------------------------------
    ! Description:
    !
    !   
    !
    ! Authors:
    !
    !   
    ! --------------------------------------------------------------------------
    
    subroutine vertical_coordinate_init(num_lev)

        integer, intent(in) :: num_lev

        character(50), parameter :: sub_name = "vertical_coordinate_init"

        allocate(hyai(num_lev+1))
        allocate(hybi(num_lev+1))
        allocate(hypi(num_lev+1))
        allocate(hyam(num_lev))
        allocate(hybm(num_lev))
        allocate(hypm(num_lev))
        allocate(sig(num_lev+1))
        allocate(sigl(num_lev))
        allocate(dsig(num_lev))

    end subroutine vertical_coordinate_init

end module vertical_coordinate
