#include <misc.h>
#include <params.h>

      SUBROUTINE TEM(U, V, PS, P, TT, HS, CB, TB, TE, TM)

      use pmgrid, only: beglatexdyn, endlatexdyn, plat
      use dynconst
      use mpi_gamil
      use mesh_params

      IMPLICIT NONE

      REAL*8 U(ilbnd:ihbnd,beglatexdyn:endlatexdyn,NL)
      REAL*8 V(ilbnd:ihbnd,beglatexdyn:endlatexdyn,NL)
      REAL*8 PS(ilbnd:ihbnd,beglatexdyn:endlatexdyn)
      REAL*8 P(ilbnd:ihbnd,beglatexdyn:endlatexdyn)
      REAL*8 TT(ilbnd:ihbnd,beglatexdyn:endlatexdyn,NL)

      REAL*8 TB(ilbnd:ihbnd,beglatexdyn:endlatexdyn,NL)
      REAL*8 CB(ilbnd:ihbnd,beglatexdyn:endlatexdyn,NL)
      REAL*8 HS(ilbnd:ihbnd,beglatexdyn:endlatexdyn)

      INTEGER I, J, K
      REAL*8  TE, EE, EK, ES, TM
      REAL*16 TMJ(8,jbeg0:jend0), EUJ(8,jbeg0:jend0), EVJ(8,jbeg0:jend0)
      REAL*16 EEJ(8,jbeg0:jend0), ESJ(8,jbeg0:jend0)
      REAL*16 TMP_EE, TMP_EK, TMP_ES, TMP_TM

!$OMP PARALLEL DO PRIVATE (I,J,K)
      DO J = jbeg0, jend0
         TMJ(1,J) = 0.0D0
         ESJ(1,J) = 0.0D0
         EUJ(1,J) = 0.0D0
         EVJ(1,J) = 0.0D0
         EEJ(1,J) = 0.0D0
         DO I = ibeg1, iend1
            TMJ(1,J) = TMJ(1,J)+PS(I,J)
            ESJ(1,J) = ESJ(1,J)+PS(I,J)*HS(I,J)
         END DO
         DO K = 1, NL
            DO I = ibeg1, iend1
               EUJ(1,J) = EUJ(1,J)+U(I,J,K)*U(I,J,K)*dV_full(ibeg1,j,k)
               EVJ(1,J) = EVJ(1,J)+V(I,J,K)*V(I,J,K)*dV_half(ibeg1,j,k)
               EEJ(1,J) = EEJ(1,J)+(CPD*PS(I,J)*TB(I,J,K)
     &              +CB(I,J,K)/CAPA*P(I,J)*TT(I,J,K))*dV_full(ibeg1,j,k)
            END DO
         END DO
      END DO
     
      TMP_EK = 0.0D0
      TMP_EE = 0.0D0
      TMP_ES = 0.0D0
      TMP_TM = 0.0D0

      DO J = jbeg0, jend0
         TMP_EK = TMP_EK+0.5d0*(EUJ(1,J)+EVJ(1,J))
         TMP_EE = TMP_EE+EEJ(1,J)
         TMP_ES = TMP_ES+ESJ(1,J)*dA_full(ibeg1,j)
         TMP_TM = TMP_TM+TMJ(1,J)*dA_full(ibeg1,j)
      END DO

      call gamil_all_reduce(TMP_EE,TMP_EK,TMP_ES,TMP_TM)

      EE = TMP_EE
      EK = TMP_EK
      ES = TMP_ES
      TM = TMP_TM
      
      TE = EK+EE+ES

      RETURN
      END
